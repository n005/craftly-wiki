[Home](/Home)\
[Install Guides](/Install-Guides)\
&nbsp;&nbsp;&nbsp;[Linux Install Script](/Linux-Install-Script)\
&nbsp;&nbsp;&nbsp;[Docker Installation](/Docker-Installation)\
[Post Installation Configuration](/Post-Installation-Configuration)\
[Starting Crafty](/Starting-Crafty)\
[Web Console](/Web-Console)\
&nbsp;&nbsp;&nbsp;[Dashboard](/Web-Console/Dashboard)\
&nbsp;&nbsp;&nbsp;[Server Control](/Web-Console/Server-Control)\
&nbsp;&nbsp;&nbsp;[Virtual Console](/Web-Console/Virtual-Console)\
&nbsp;&nbsp;&nbsp;[Logs](/Web-Console/Logs)\
&nbsp;&nbsp;&nbsp;[Backups](/Web-Console/Backups)\
&nbsp;&nbsp;&nbsp;[Scheduled Tasks](/Web-Console/Scheduled Tasks)\
&nbsp;&nbsp;&nbsp;[Config](/Web-Console/Config)\
[How Secure is Crafty?](https://craftycontrol.com/docs/crafty-security/)
