### Pre 3.0 

La première fois que Crafty est exécuté, il posera à l'utilisateur une série de questions pour aider à configurer le produit. Les réponses à ces questions peuvent être modifiées ultérieurement si nécessaire via la console Web sous la zone de configuration.

Le programme d'installation a des valeurs par défaut et appuyer sur Entrée sans rien saisir acceptera les valeurs par défaut. À la fin de l'installateur, Crafty démarrera en utilisant les paramètres définis lors de *Crafty configuration installer*. Il attendra alors que l'utilisateur aillent sur la console et entre la commande de d'arrêt du serveur ("stop" ou "exit") et sinon il continuera à s'exécuter jusqu'à ce qu'une telle commande soit donnée. Le programme d'installation crée automatiquement un nom d'utilisateur (Admin) et un mot de passe aléatoire à 6 caractères. Veuillez modifier le mot de passe immédiatement via la console Web.

### Post 3.0

La première fois que Crafty est exécuté, il posera à l'utilisateur une série de questions pour aider à configurer le produit. Les réponses à ces questions peuvent être modifiées ultérieurement si nécessaire via la console Web sous la zone de configuration. Ces questions seront présentées lors de la première connexion à l'interface de gestion, plutôt que dans la console comme les versions précédentes.

Pour vous connecter à l'interface de gestion, vous devrez saisir votre navigateur et vous diriger vers `https://<l'ip du serveur>:8000/` et vous connecter avec le nom d'utilisateur `Admin` et le mot de passe fournis dans la console Crafty.

Dans ce cas, le programme d'installation n'a pas de valeurs par défaut, vous devrez donc saisir manuellement les paramètres souhaités. Une fois le programme d'installation terminé, Crafty démarre en utilisant les paramètres que vous avez définis. Veuillez modifier le mot de passe immédiatement via la console Web.
&nbsp;  
### Options d'installation

| **Question** | **Description** | **Par défaut / suggéré (3.0 et supérieur)** |
| ------ | ------ | ------ |
| *Dans quel dossier se trouve le fichier .jar du serveur?* | Entrez ici où vous souhaitez que Crafty recherche votre * .jar lors du démarrage de votre serveur. | `/var/opt/minecraft/server` |
| *Quel est le nom de fichier de votre server.jar ??* | Entrez le nom de votre serveur * .jar exactement comme il est écrit, c'est ce que Crafty cherchera à exécuter. | `paperclip.jar` |
| *Server **Maximum** Memory* | Entrez la quantité maximale de mémoire que vous souhaitez que votre * .jar consomme. Ceci est différent de la mémoire totale du système et n'est pas configuré par Crafty. | `2048` |
| *Server **Minimum** Memory* | Entrez la quantité minimale de mémoire que vous souhaitez que votre * .jar consomme. Ceci est différent de la mémoire totale du système et n'est pas configuré par Crafty. *Remarque, les saveurs Minecraft nécessitent souvent au moins 512 Mo*. | `1024` |
| *Additional Arguments* | Si vous souhaitez que votre server.jar soit exécuté avec des arguments supplémentaires, entrez-les ici. | n/a |
| *Server Autostart* | Si vous souhaitez que votre serveur démarre automatiquement au démarrage de Crafty, entrez `y`. Sinon, entrez `n`. | `y` |
| *Autostart Delay* | Si vous souhaitez que Crafty attende un peu avant de démarrer votre serveur, entrez le temps en secondes ici. | `10` |
| *What port should the webserver run on?* | Fournissez le port sur lequel vous souhaitez vous connecter à Crafty WebUI. | `8000` |