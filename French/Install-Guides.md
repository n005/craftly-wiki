Ici, nous avons quelques infos pour vous aider à mettre en place Crafty Controller.

> Nous prenons actuellement en charge Windows 7/8/10, Ubuntu 18.04 / 19.04 et macOS 10.14+.
> Bien que d'autres distributions Linux puissent fonctionner, nous ne les prenons pas officiellement en charge.

## Sélectionnez un guide d'installation
*  [Installer Crafty sur Windows](#installer-crafty-sur-windows)
*  [Installer Crafty sur Linux](#installer-crafty-sur-linux)
*  [Installer Crafty sur macOS](#installer-crafty-sur-macos)

&nbsp;  
&nbsp;  

## Installer Crafty sur Windows

[Need help? Click here!](home/French/#get-help-installing-or-using-crafty)

#### Exigences
Crafty, comme avec tous les gestionnaires de serveurs, nécessite que votre serveur soit dans un état opérationnel. Les dossiers Eula.txt et world sont déjà créés. N'essayez pas d'exécuter Crafty avec seulement un fichier jar dans votre dossier de serveur, cela plantera.

#### Installation
1.  Téléchargez Crafty.
C'est facile. Accédez à la page des versions et téléchargez la dernière version de Crafty pour votre plate-forme.
2.  Décompressez Crafty.
3.  Double-cliquer sur ‘run_crafty.bat’.
4.  Crafty devrait maintenant démarrer et lancer le programme d'installation de Crafty.
5.  Suivez les instructions pour configurer Crafty sur votre environnement.
6.  Ça marche !

&nbsp;  
&nbsp;  
## Installer Crafty sur Linux

[Need help? Click here!](home/#get-help-installing-or-using-crafty)

> Vous n'avez pas envie d'installer manuellement? Pourquoi ne pas consulter notre [installation script](/French/Linux-Install-Script)?

##### Exigences et hypothèses
*  Vous avez sudo et pouvez installer des logiciels
* Ce guide suppose que vous avez une bonne compréhension d'un environnement Linux et que vous êtes à l'aise avec la ligne de commande.
* Ce guide suppose également que vous installerez Crafty dans `/var/opt/minecraft/crafty`.
* Nous supposons également que votre serveur sera dans `/var/opt/minecraft/crafty`.
* Nous supposons également que le compte utilisateur exécutant crafty aura des autorisations complètes de lecture / écriture / exécution sur ces dossiers.
* Ce guide vous expliquera également comment configurer un compte de service pour Crafty.
* Enfin, cette guilde suppose que les logiciels suivants soit installés et à jour: Git, Python 3.7, Python 3.7-dev, python3-pip.
&nbsp;  

#### Installation

##### 1. Installer les logiciels requis
Sur les variantes Ubuntu / Debian, vous pouvez installer le logiciel requis via la ligne de commande en tapant :

```
sudo apt install git python3.7 python3.7-dev python3-pip software-properties-common
```
&nbsp;  
##### 2. Créer un répertoire pour Crafty
Faisons un répertoire où Crafty pourra être.
Ce guide utilisera `/var/opt/minecraft/crafty` comme exemple.

```
sudo mkdir -p /var/opt/minecraft/crafty
```
&nbsp;  
##### 3.  Créez un compte utilisateur Crafty
```
sudo useradd crafty
```
&nbsp;  
##### 4.  Autorisations d'installation pour les dossiers
*Vous pouvez configurer le groupe que vous souhaitez, mais ce guide utilise nogroup.*

```
sudo chown crafty:nogroup -R /var/opt/minecraft/crafty
sudo chown crafty:nogroup -R /var/opt/minecraft/server
```
&nbsp;
##### 5.  Changer pour le répertoire Crafty
```
cd /var/opt/minecraft/crafty
```
&nbsp;  
##### 6.  Clone Crafty Repo
*Veuillez vous assurer d'être dans le dossier `crafty` avant de cloner le dépôt.*
```
git clone https://gitlab.com/Ptarrant1/crafty-web.git
```
&nbsp;  
##### 7.  Installer Virtualenv à l'aide de PIP
```
sudo pip3 install virtualenv
```
&nbsp;  
##### 8.  Créer un environnement virtuel
```
virtualenv venv
```
&nbsp;   
##### 9.  Activer l'environnement virtuel
```
source venv/bin/activate
```
&nbsp;  
##### 10.  Allez dans le dossier crafty-web qui a été cloné
```
cd /var/opt/minecraft/crafty-web
```
&nbsp;  
##### 11.  Installez toutes les choses / exigences
```
pip3 install -r requirements.txt
```
&nbsp;  
##### 12.  Assurez-vous que Java est installé, nous utilisons Open-jdk
```
sudo apt install openjdk-8-jdk opendjdk-8-jre
```
&nbsp;  
##### 13.  Exécutez Crafty:
```
python crafty.py
```
&nbsp;  
**C'est bon ! Crafty devrait maintenant être en cours d'exécution et vous pouvez nous poser des questions sur l'installation.**

&nbsp;  
&nbsp;  

## Installer Crafty sur macOS

Ces instructions vous guideront pour installer Crafty sur macOS. Ce guide a été conçu en tenant compte de macOS Catalina 10.15.2, mais il devrait fonctionner pour la plupart dans macOS Mojave 10.14.6 et macOS High Sierra 10.13.6. Tout ce qui est plus ancien ne le sera probablement pas et nous ne le prendrons pas en charge ici.

##### Exigences et hypothèses

* Vous aurez besoin de brew.sh installé sur votre Mac. Obtenez-le de [https://brew.sh](https://brew.sh)
* Ce guide suppose que vous avez une bonne compréhension de l'environnement du terminal macOS et que vous êtes à l'aise avec la ligne de commande.
* Ce guide suppose que vous installerez Crafty dans `/var/opt/minecraft/crafty`.
* Ce guide suppose que votre server.jar est situé dans `/var/opt/minecraft/server`.
* Nous supposons également que le compte utilisateur exécutant crafty aura des autorisations complètes de lecture / écriture / exécution sur ces dossiers.

#### Installation

##### 1. Installer les prérequis

###### Brew.sh

macOS ne dispose pas d'un gestionnaire de packages approprié et pour installer quelques logiciels requis via la ligne de commande, vous aurez besoin de Brew.sh.

Allez dans `/Applications/Utilities` et lancer Terminal.app ou appuyez sur Command+Space et tapez Terminal.app et appuyez sur la touche retour / entrée.  Collez ensuite la ligne ci-dessous:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

La commande ci-dessus s'exécute et vous verrez la progression de l'installation. Il vous informera que le *Command Line Tools for Xcode* sera installé. 

Vers la fin, il vous sera demandé d'entrer votre *mot de passe de compte*, puis l'installation se poursuivra.

Vous serez averti lorsque Brew commencera à télécharger les *outils de ligne de commande pour Xcode* et selon votre connexion, le téléchargement prendra environ 5 minutes. Une fois les outils téléchargés, ils s'installeront sur votre Mac et l'installation de Brew sera terminée.

###### Installation des packages prérequis
Ce guide suppose que les logiciels suivants sont installés et à jour: Git, Python 3.7 et Java (Open JDK est ok).

Tout d'abord, installez Git et Python 3.75
```
brew install git python3
```
Ensuite OpenJDK
```
brew cask install java
```
Puis upgrade pip
```
pip install --upgrade pip
```

##### 2. Installation de Crafty

###### Créer un répertoire pour Crafty

Faisons un répertoire où Crafty pourra être. Ce guide utilisera `/var/opt/minecraft/crafty` comme exemple.

```
sudo mkdir -p /var/opt/minecraft/crafty
```

###### Autorisations d'installation pour le dossier

```
sudo chown <your_username>:admin /var/opt/minecraft/crafty
```

**Par exemple** Si votre nom d'utilisateur sur votre mac est **Totoro** alors la commande sera: 
`sudo chown totoro:admin /var/opt/minecraft/crafty`

######  Changer vers le répertoire Crafty

```
cd /var/opt/minecraft/crafty
```

###### Créer un environnement virtuel "venv"

```
python3 -m venv venv
```

######  Clone Crafty Repo

*Assurez-vous d'être dans le dossier `crafty` avant de cloner le repo.* Pour vérifier `pwd` et assurez-vous qu'il est dit `/var/opt/minecraft/crafty` avant de continuer.

```
git clone https://gitlab.com/Ptarrant1/crafty-web.git
```
Switcher vers le dossier `crafty-web`
```
cd /var/opt/minecraft/crafty/crafty-web
```
Vérifions ensuite que nous sommes sur la dernière snapshot et checkout
```
git pull
git checkout snapshot
```

Allez vers le repertoire
```
cd ..
```

######  Activer l'environnement virtuel
Permet d'activer l'environnement virtuel

```
source venv/bin/activate
```
votre invite passera à `(venv) quelquechose is:`

######  Allez dans le dossier crafty-web qui a été cloné

```
cd /var/opt/minecraft/crafty-web
```

######  Installez toutes les choses / exigences

```
pip install -r requirements.txt
```

######  Lancer Crafty:

```
python crafty.py
```

**C'est ça! Crafty devrait maintenant fonctionner sur macOS Catalina et n'hésiter pas à nous poser des questions à propos de l'installation.**