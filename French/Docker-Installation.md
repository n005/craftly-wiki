# Installation via Docker

## Prérequis

1. La dernière version Docker, vous pouvez l'obtenir [Ici](https://www.docker.com/get-started)
2. La dernière version de [docker-compose](https://github.com/docker/compose)
3. `git` installé sur votre système

## Installation

Tout d'abord, permet de cloner le repo crafty-web dans le répertoire de votre choix. Pour ce tutoriel, j'utiliserai `/opt/minecraft`.

Ouvrez une fenêtre shell et tapez:

```bash
mkdir /opt
cd /opt
git clone https://gitlab.com/crafty-controller/crafty-web minecraft
cd minecraft
git checkout 3.0
```

Ensuite, placez les fichiers JAR de votre serveur minecraft dans `docker/minecraft_servers`.

Une fois cela fait, exécutez le conteneur

#### Mode premier plan

```bash
docker-compose up
```

#### Mode d'arrière-plan

```bash
docker-compose up -d
```

Ensuite, accédez simplement à Crafty comme vous le feriez normalement. Lorsque vous spécifiez le répertoire du serveur minecraft, veuillez utiliser `/minecraft_servers`
