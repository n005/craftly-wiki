Le démarrage de Crafty peut changer en fonction de votre système d'exploitation et de votre configuration. Cependant, voici les moyens les plus courants:

## Windows
*  Après avoir téléchargé et extrait crafty, double-cliquez sur `run_crafty.bat` dans le dossier.
* Crafty doit être exécuté à partir du même dossier que ses bibliothèques et autres ressources.
* Ce fichier batch sera automatiquement orienté (cd) vers le bon répertoire (`crafty`) et exécutera crafty.exe. Si vous le souhaitez, vous pouvez le faire vous-même via une interface de ligne de commande.

## Linux
Nous vous recommandons d'utiliser une sorte de service de fenêtrage de terminal pour permettre à Crafty de continuer à fonctionner même après la fermeture de votre session de terminal (par exemple, lorsqu'il est connecté à votre serveur via ssh). Nous avons écrit des instructions pour deux applications populaires, screen et tmux.

#### Via Screen
1. À partir d'un terminal, tapez `screen -S crafty` et appuyez sur Entrée.
   - Cela va créer une nouvelle session d'écran appelée *crafty*.
2. `cd` dans le répertoire où crafty a été installé, par exemple `/var/opt/minecraft/crafty/web-crafty`.
3. Entrez maintenant`./run_crafty.sh` et appuyez sur Entrée.
4. Crafty va maintenant se lancer.
5. Vous pouvez vous *detacher* ce cette session screen en appuyant `CTRL+A+D`
   - Vous pouvez vous rattacher à cet écran (par exemple pour quitter Crafty) en tapant `screen -R crafty`.

En savoir plus sur *screen* [ici](https://hostpresto.com/community/tutorials/how-to-use-screen-on-linux/).

#### Via tmux
1. À partir d'un terminal, tapez `tmux` et appuyez sur Entrée.
   - Cela va créer une nouvelle session d'écran appelée *crafty*.
2. `cd` dans le répertoire où crafty a été installé, par exemple `/var/opt/minecraft/crafty/web-crafty`.
3. Maintenant, lancez `./run_crafty.sh` à partir de Tmux. 
4. Crafty se lancera. Une fois que Crafty a terminé le lancement, vous pouvez le « détacher » en appuyant sur «CTRL + B» puis sur «CTRL + D» pour détacher la fenêtre.
   - Pour rattacher la fenêtre tapez `tmux attach 0`. 

En savoir plus sur *tmux* [ici](https://thoughtbot.com/blog/a-tmux-crash-course).

#### Démarrage automatique de Crafty au redémarrage
Actuellement, Crafty doit être démarré manuellement chaque fois que vous allumez votre machine. Pendant que nous travaillons à faire fonctionner Crafty en tant que service, vous pouvez [suivre les instructions listées ici](https://gitlab.com/Ptarrant1/crafty-web/issues/22#note_255441321) pour que Crafty démarre automatiquement au démarrage.

#### Post 3.0
Vous pouvez spécifier le `-d` option lors de l'exécution `crafty.py`, pour le daemonizez. 
```bash
python crafty.py -d
```

Il existe également un fichier de configuration de service situé dans `configs/`, qui peut être utilisé en mettant l'option `-c` lors de l'exécution de `crafty.py`. **Cela désactivera la console dans Crafty**
```bash
python crafty.py -c configs/service_config.yml
```

