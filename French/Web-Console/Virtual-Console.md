*Parfois, vous avez juste besoin d'un terminal.*

La console virtuelle vous donne accès à l'invite de commande de votre serveur et est mise à jour une fois par seconde. Pour envoyer une commande, tapez simplement votre commande et appuyez sur Entrée (ou appuyez sur «send command»).

C’est tout, c’est vraiment aussi simple que cela.

![virtual_console_1_](uploads/dc725a30cea13ba9c83c0737aa2c552e/virtual_console_1_.png)