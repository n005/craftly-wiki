Cette page affiche plusieurs journaux de votre système, par opposition au seul `latest.log` par défaut de votre serveur. Tous les journaux sont présentés dans une interface à onglets, *le plus récent* en bas. Pour faire défiler rapidement vers le bas, cliquez simplement dans la zone de texte et il devrait vous faire défiler automatiquement vers le bas.

Pour des raisons de performances, ces journaux sont tronqués sur les 100 dernières lignes environ, à l'exception du dernier journal. Le dernier fichier journal est coupé aux 300 dernières lignes. En bas se trouvent toutes les lignes d'avertissement ou d'erreur trouvées dans `latest.log` dans un tableau facile à rechercher. Le numéro de ligne est répertorié avec l'élément de journal afin que vous puissiez facilement trouver ces lignes dans vos journaux.

*  Latest.log – Il s'agit du journal principal de votre serveur. C'est aussi occupé que votre serveur.
*  Crafty.log – Ceci est le journal Crafty Controllers. C'est assez calme à moins que Crafty ne fasse quelque chose.
*  Schedule.log – Ceci est le journal du planificateur. Ceci est un journal très chargé. Access.log - Ce sont les journaux d'accès à votre interface Crafty.
*  Access.log – Ce sont des journaux d'accès Tornado. Tornado est le serveur Web derrière Crafty.

![logs-b2_1_](uploads/1a99a4a54a830261040e395803db1358/logs-b2_1_.png)