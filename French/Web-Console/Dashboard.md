Après vous être connecté à la console, le tableau de bord Crafty devrait vous être présenté. Il y a beaucoup de données ici, alors commençons en haut à gauche et continuons sur toute la page web.

### Statistiques d'utilisation
La première chose que vous devriez remarquer est les 4 cases situées en haut du CPU, de la mémoire, du disque et des lecteurs. Ce sont les statistiques d'utilisation de votre système complet, pas uniquement ce que Crafty utilise. Le nombre de joueurs est mis à jour toutes les 10 secondes et affichera 0 si votre serveur n'est pas démarré.

### Détails du système
Sous les quatre principales statistiques d'utilisation en haut de la page, vous devriez voir une boîte qui occupe environ la moitié de l'écran. Il s'agit de la zone des détails du système qui indique si le serveur fonctionne avec un pouce en l'air![20iFphN_1_](uploads/369c09b2482b31c36ccd39c43838ef27/20iFphN_1_.png), ou pas avec un pouce vers le bas![TBGhM7a_1_](uploads/d28c467b7fc26b5602f1e84e048c0911/TBGhM7a_1_.png). De plus, la version du serveur, le message du jour (MOTD) et d'autres statistiques sont ici, y compris le nom du monde.

### Données historiques
À droite des détails du système, vous devriez voir le graphique historique, qui montre les statistiques de votre système au fil du temps. Par défaut, les données sont enregistrées toutes les 60 minutes et conservées pendant 1 jour, mais vous pouvez les modifier via la page "Config". En survolant un point dans le temps, le CPU, la mémoire et les lecteurs seront affichés en ligne pour cette période. C'est un excellent endroit pour voir les performances de votre serveur au fil du temps. Nous conservons ces données dans le fichier crafty.sqlite.
![dashboard-b2-1024x507_1_](uploads/44812e97cc9e7fedd87470cfacf8a807/dashboard-b2-1024x507_1_.png)