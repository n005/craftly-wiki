Crafty a un système de sauvegarde intégré qui peut être configuré via la page [configuration](/Web-Console/Config). Le paramètre par défaut consiste à sauvegarder l'intégralité du dossier du serveur et à le déplacer dans le même répertoire que Crafty. Où les sauvegardes sont stockées sont également configurables via la page de configuration. La page des sauvegardes comporte un simple bouton «Sauvegarder maintenant» ("backup now") ainsi qu'une liste des sauvegardes déjà trouvées sur le système. 

> **Attention:** ces fichiers sont volumineux et leur téléchargement peut prendre un certain temps.

Appuyez sur le bouton Supprimer pour ouvrir une boîte de confirmation pour vous assurer que vous souhaitez supprimer la sauvegarde sélectionnée.

![backups_1_](uploads/b2d9d9360d044d89b03576ffee9ece92/backups_1_.png)