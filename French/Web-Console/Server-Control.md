La page de contrôle du serveur est une page très simple avec seulement 3 boutons: Démarrer(start), Arrêter(stop) ou Redémarrer(restart).

**Start** est désactivé si le serveur est déjà démarré.

**Restart** est désactivé s'il n'y a rien à redémarrer.

**Stop** est désactivé s'il n'y a rien à arrêter. 

&nbsp;  
En raison de la manière dont **restart** est mis en œuvre et des temps d'attente dans cette procédure, son exécution prend plus de temps qu'une simple procédure de démarrage / arrêt.

&nbsp;
![Server_actions](uploads/982f4f9bb4f50327e6df27847c8500f2/Server_actions_1_.png)