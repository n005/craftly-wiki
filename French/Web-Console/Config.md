Cette page a une interface à onglets et affiche certains paramètres supplémentaires en plus des paramètres du serveur Minecraft.

> **Attention:** La suppression de la base de données .sqlite supprimera tous les utilisateurs et paramètres configurés dont vous disposez, et sera essentiellement la même chose que de recommencer à zéro.

### Onglet Crafty Config
Ici, vous pouvez ajuster les paramètres de capture et de sauvegarde des données historiques, ainsi que le port sur lequel Crafty Web s'exécute.

### Onglet Backups
Cet onglet vous permet de configurer les paramètres de sauvegarde pour Crafty.

### Onglet MC Server
Cet onglet vous permet d'ajuster les paramètres de démarrage de votre serveur.

### Onglet Users
L'onglet utilisateur vous permet de configurer des utilisateurs supplémentaires pour accéder au panneau et leur *rôle*. Reportez-vous au tableau ci-dessous pour une description de chaque rôle:

| Rôle | Permissions |
| ------ | ------ |
| Admin | Accès complet à tout dans l'interface utilisateur Crafty Web. |
| Mod | Accès à la page Journaux. |
| Backup | Identique au rôle Mod, mais a également la possibilité de modifier l'onglet Sauvegarde dans la page de configuration. |
| Staff | Tout ce que la sauvegarde peut faire, ainsi que l'accès à la page Planification(Tasks). | 

Actuellement, l'utilisateur Admin ne peut pas être supprimé ou modifié en dehors du mot de passe.