# Bienvenue dans la documentation!

> Ce wiki est en construction et les choses peuvent changer significativement. De plus, comme il s'agit d'un projet très actif, alors ... les choses peuvent changer très rapidement :).

### Contenu
[Accueil Anglais](/Home)\
[Guides d'installations](/French/Install-Guides)\
&nbsp;&nbsp;&nbsp;[Script d'installation Linux](/French/Linux-Install-Script)\
&nbsp;&nbsp;&nbsp;[Installation via Docker](/French/Docker-Installation)\
[Configuration Post-Installation](/French/Post-Installation-Configuration)\
[Démarrer Crafty](/French/Starting-Crafty)\
[Console Web](/French/Web-Console)\
&nbsp;&nbsp;&nbsp;[Tableau de bord](/French/Web-Console/Dashboard)\
&nbsp;&nbsp;&nbsp;[Controle du Serveur](/French/Web-Console/Server-Control)\
&nbsp;&nbsp;&nbsp;[Console Virtuel](/French/Web-Console/Virtual-Console)\
&nbsp;&nbsp;&nbsp;[Logs](/French/Web-Console/Logs)\
&nbsp;&nbsp;&nbsp;[Backups](/French/Web-Console/Backups)\
&nbsp;&nbsp;&nbsp;[Tâches planifiées (Tasks)](/French/Web-Console/Scheduled Tasks)\
&nbsp;&nbsp;&nbsp;[Config](/French/Web-Console/Config)\
[Quelle est la sécurité de Crafty? <sup>external</sup>](https://craftycontrol.com/docs/crafty-security/)\
[Traduction]()\
&nbsp;&nbsp;&nbsp;[Français](/French/Home) <sup>Vous êtes ici</sup>


&nbsp;  

#### Obtenez de l'aide pour installer ou utiliser Crafty

*  Visitez notre [Discord](https://discord.gg/S8Q3AKb)!

&nbsp;  

#### Besoin de testeurs bêta!

* Allez sur notre Discord en utilisant le lien ci-dessus pour vous inscrire et nous aider à tester Crafty. Nous recherchons des personnes avec les systèmes suivants :
    * Ubuntu 18.04, Ubuntu 19.04
    * Windows 7, 8, 10
    * macOS 10.14+
