# Welcome to the docs!

> This wiki is under construction, and things may change significantly. Additionally, as this is a very active project, so... things may change significantly :) .

### Contents
[Home](/Home) <sup>You are here!</sup>\
[Install Guides](/Install-Guides)\
&nbsp;&nbsp;&nbsp;[Linux Install Script](/Linux-Install-Script)\
&nbsp;&nbsp;&nbsp;[Docker Installation](/Docker-Installation)\
[Post Installation Configuration](/Post-Installation-Configuration)\
[Starting Crafty](/Starting-Crafty)\
[Web Console](/Web-Console)\
&nbsp;&nbsp;&nbsp;[Dashboard](/Web-Console/Dashboard)\
&nbsp;&nbsp;&nbsp;[Server Control](/Web-Console/Server-Control)\
&nbsp;&nbsp;&nbsp;[Virtual Console](/Web-Console/Virtual-Console)\
&nbsp;&nbsp;&nbsp;[Logs](/Web-Console/Logs)\
&nbsp;&nbsp;&nbsp;[Backups](/Web-Console/Backups)\
&nbsp;&nbsp;&nbsp;[Scheduled Tasks](/Web-Console/Scheduled Tasks)\
&nbsp;&nbsp;&nbsp;[Config](/Web-Console/Config)\
[How Secure is Crafty? <sup>external</sup>](https://craftycontrol.com/docs/crafty-security/)\
[Translation]()\
&nbsp;&nbsp;&nbsp;[French](/French/Home)

&nbsp;  

#### Get help installing or using Crafty

*  Visit our [Discord](https://discord.gg/S8Q3AKb)!

&nbsp;  

#### Beta testers needed!

* Hop on our Discord using the link above to sign up and help us test Crafty. We're looking for people with the following systems:
    * Ubuntu 18.04, Ubuntu 19.04
    * Windows 7, 8, 10
    * macOS 10.14+
