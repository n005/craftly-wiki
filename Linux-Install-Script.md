For a quick one-liner install, copy and paste the below into your terminal.

```
wget https://craftycontrol.com/wp-content/uploads/2019/11/install_crafty.sh_.zip && unzip install_crafty.sh_.zip && chmod +x install_crafty.sh
```