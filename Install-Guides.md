Here we have a few guides to help you get Crafty Controller up and running.

> We currently support Windows 7/8/10, Ubuntu 18.04/19.04, and macOS 10.14+.
> While other linux distro's may work, we don't officially support them.

## Select an install guide
*  [Install Crafty on Windows](#install-crafty-on-windows)
*  [Install Crafty on Linux](#install-crafty-on-linux)
*  [Install Crafty on macOS](#install-crafty-on-macos)

&nbsp;  
&nbsp;  

## Install Crafty on Windows

[Need help? Click here!](home/#get-help-installing-or-using-crafty)

#### Requirements
Crafty, as with all server managers, requires your server to be in a operational state. Eula.txt and world folders are already created. Don't try to run Crafty with only a jar file in your server folder, it will crash.

#### Installation
1.  Download Crafty.
This is easy. Go to the releases page and download the latest version of Crafty for your platform.
2.  Unzip Crafty.
3.  Double-click ‘run_crafty.bat’.
4.  Crafty should now launch and present the Crafty installer.
5.  Follow the prompts to get Crafty setup for your environment.
6.  That's it!

&nbsp;  
&nbsp;  
## Install Crafty on Linux

[Need help? Click here!](home/#get-help-installing-or-using-crafty)

> Don't feel like installing manually? Why not check out our [installation script](/Linux-Install-Script)?

##### Requirements & Assumptions
*  You have sudo and can install software
*  This guide assumes you have a decent understanding of a Linux environment and are comfortable with the command line.
*  This guide also assumes you will be installing Crafty into `/var/opt/minecraft/crafty`.
*  We also assume your server will be at `/var/opt/minecraft/server`.
*  We also assume the user account running crafty will have full read/write/execute permissions on these folders.
*  This guide will instruct you how to setup a service account for Crafty as well.
*  Finally this guild assumes you have the following software installed and up to date: Git, Python 3.7, Python 3.7-dev, python3-pip.

&nbsp;  
#### Installation

##### 1. Install Required Software
On Ubuntu/Debian variants, you can install required software via the command line by typing:

```
sudo apt install git python3.7 python3.7-dev python3-pip software-properties-common
```
&nbsp;  
##### 2.  Create a directory for Crafty
Let’s make crafty a place to live on your server.
This guide will use `/var/opt/minecraft/crafty` as it’s example.

```
sudo mkdir -p /var/opt/minecraft/crafty
```
&nbsp;  
##### 3.  Create a Crafty User Account
```
sudo useradd crafty
```
&nbsp;  
##### 4.  Setup permissions for the folders
*You can setup whichever group you wish, but this guide uses nogroup.*

```
sudo chown crafty:nogroup -R /var/opt/minecraft/crafty
sudo chown crafty:nogroup -R /var/opt/minecraft/server
```
&nbsp;
##### 5.  Change to the Crafty dir
```
cd /var/opt/minecraft/crafty
```
&nbsp;  
##### 6.  Clone Crafty Repo
*Please be sure to be in the `crafty` folder before cloning the repo.*
```
git clone https://gitlab.com/Ptarrant1/crafty-web.git
```
&nbsp;  
##### 7.  Install Virtualenv using PIP
```
sudo pip3 install virtualenv
```
&nbsp;  
##### 8.  Create a Virtual Environment
```
virtualenv venv
```
&nbsp;   
##### 9.  Activate the Virtual Environment
```
source venv/bin/activate
```
&nbsp;  
##### 10.  Go into the crafty-web folder that was cloned down
```
cd /var/opt/minecraft/crafty-web
```
&nbsp;  
##### 11.  Install all the things / requirements
```
pip3 install -r requirements.txt
```
&nbsp;  
##### 12.  Be sure Java is installed, I use Open-jdk
```
sudo apt install openjdk-8-jdk opendjdk-8-jre
```
&nbsp;  
##### 13.  Run Crafty:
```
python crafty.py
```
&nbsp;  
**That’s it! Crafty should now be running and asking some install questions.**

&nbsp;  
&nbsp;  

## Install Crafty on macOS

These instructions will guide you to install Crafty on macOS.  This guide was built taking macOS Catalina 10.15.2 in mind, but it should work for the most part in macOS Mojave 10.14.6 and macOS High Sierra 10.13.6.  Anything older it probably won't and we won't cover it here.

##### Requirements & Assumptions

* You will need brew.sh installed in your mac. Get it from [https://brew.sh](https://brew.sh)
* This guide assumes you have a decent understanding of the macOS Terminal environment and are comfortable with the command line.
* This guide assumes you will be installing Crafty into `/var/opt/minecraft/crafty`.
* This guide assumes your server.jar is located in `/var/opt/minecraft/server`.
* We also assume the user account running crafty will have full read/write/execute permissions on these folders.

#### Installation

##### 1. Install Pre-Requisites

###### Brew.sh

macOS lacks a proper package manager and in order to install a couple of the required software via the command line you will need Brew.sh.

Go to your `/Applications/Utilities` and launch Terminal.app or press Command+Space and type Terminal.app and hit return/enter key.  Then paste the line below:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

The command above runs and you will see the installation progress.  It will inform you that the *Command Line Tools for Xcode* will get installed. 

Towards the end you will be asked to enter your *account password* then the installation will continue.

You will be notified when Brew begins downloading the *Command Line Tools for Xcode* and depending on your connection the download will take about 5 minutes.  Once the tools have downloaded, they will install on your mac and the installation of Brew will be completed.

###### Installing Pre-Requisite Packages
This guide assumes you have the following software installed and up to date: Git, Python 3.7, and Java (Open JDK is fine).

First, install Git and Python 3.75
```
brew install git python3
```
then OpenJDK
```
brew cask install java
```
then upgrade pip
```
pip install --upgrade pip
```

##### 2. Installing Crafty

###### Create a directory for Crafty

Let’s make crafty a place to live on your server. This guide will use `/var/opt/minecraft/crafty` as it’s example:

```
sudo mkdir -p /var/opt/minecraft/crafty
```

###### Setup permissions for the folder

```
sudo chown <your_username>:admin /var/opt/minecraft/crafty
```

**For Example** If your username on your mac is **Totoro** then the command will be: 
`sudo chown totoro:admin /var/opt/minecraft/crafty`

######  Change to the Crafty dir

```
cd /var/opt/minecraft/crafty
```

###### Create a virtual environment "venv"

```
python3 -m venv venv
```

######  Clone Crafty Repo

*Please be sure to be in the `crafty` folder before cloning the repo.* To check type `pwd` and make sure it says `/var/opt/minecraft/crafty` before you continue.

```
git clone https://gitlab.com/Ptarrant1/crafty-web.git
```
Switch to the `crafty-web` directory
```
cd /var/opt/minecraft/crafty/crafty-web
```
Then lets make sure we are on the latest snapshot and lets checkout
```
git pull
git checkout snapshot
```

Let's go up one directory
```
cd ..
```

######  Activate the Virtual Environment
Lets activate the Virtual Environment

```
source venv/bin/activate
```
your prompt will change to `(venv) whateveryourprompt is:`

######  Go into the crafty-web folder that was cloned down

```
cd /var/opt/minecraft/crafty-web
```

######  Install all the things / requirements

```
pip install -r requirements.txt
```

######  Run Crafty:

```
python crafty.py
```

**That’s it! Crafty should now be running on macOS Catalina and asking some install questions.**
