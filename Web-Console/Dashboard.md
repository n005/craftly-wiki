After logging into the console, you should be presented with the Crafty Dashboard. There is a lot of data here, so let’s start top left and work our way across.

### Usage Stats
The first thing you should notice is the 4 boxes at the top CPU, Memory, Disk, and Players. These are the usage statistics of your host as a whole, not what Crafty is using of the host system. The players count is updated every 10 seconds and will show 0 if your server is not started.

### System Details
Below the four main usage stats at the top of the page, you should see a box that takes up about 1/2 the screen. This is the system details area which shows if the server is running with a thumbs up ![20iFphN_1_](uploads/369c09b2482b31c36ccd39c43838ef27/20iFphN_1_.png) icon, or thumbs down ![TBGhM7a_1_](uploads/d28c467b7fc26b5602f1e84e048c0911/TBGhM7a_1_.png) icon. Additionally, the server version, message of the day (MOTD) and other stats are here including the level name.

### Historical Data
To the right of the system details you should see the historical graph, which shows your system stats over time. By default the data is recorded every 60 minutes and kept for 1 day, but you can change this via the Config page. Hovering over a point in time will show the CPU, Memory, and Players online for that time period. This is a great place to see how your server is performing over time. We keep this data in the crafty.sqlite file.

![dashboard-b2-1024x507_1_](uploads/44812e97cc9e7fedd87470cfacf8a807/dashboard-b2-1024x507_1_.png)