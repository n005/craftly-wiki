*Sometimes you just need a terminal.*

The Virtual Console gives you access to your server’s command prompt and is updated once per second. To send a command, simply type your command and hit enter (or press “send command”). 

That’s it, it’s really that simple.

![virtual_console_1_](uploads/dc725a30cea13ba9c83c0737aa2c552e/virtual_console_1_.png)