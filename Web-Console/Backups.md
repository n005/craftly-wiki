Crafty has a built in backup system that can be configured via the [config](/Web-Console/Config) page. The default setting is to backup the entire server folder and move it under the same directory as Crafty. Where the backups are stored are also configurable via the config page. The backups page has a simple “backup now” button as well as a listing of backups that are already found on the system. 

> **Warning:** these files are large and can take some time to download. 

Pressing the delete button will launch a confirmation box to make sure you wish to delete the backup selected.

![backups_1_](uploads/b2d9d9360d044d89b03576ffee9ece92/backups_1_.png)