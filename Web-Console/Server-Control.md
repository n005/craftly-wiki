The server control page is a very simple page with only 3 buttons: Start, Stop, or Restart. 

**Start** is disabled if the server is already started

**Restart** is disabled if there's nothing to restart.

**Stop** is disabled if there's nothing to stop. 

&nbsp;  
Due to the way **restart** is implemented and the waiting times in that procedure, it takes longer to execute than a simple start / stop procedure.

&nbsp;
![Server_actions](uploads/982f4f9bb4f50327e6df27847c8500f2/Server_actions_1_.png)